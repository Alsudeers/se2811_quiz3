/**
 * Web Application Development - SE 2811
 * Quiz 3
 * 1/9/2020
 * Names:
 *      Sara Alsudeer
 *      Lucas Cabello
 *      David Yang
 */


/**
 * Class cell handles initializing cells and drawing colors on cells.
 */
class Cell{

    constructor() {

        /**
         * Draws the color on the cell
         * @param drawing - The drawing element
         * @param cell - the cell to draw the color on
         */
        const
            doDraw = (drawing, cell) => {
                cell.style.backgroundColor = drawing.selectedColor;
            };

        /**
         * creates and initializes a new cell
         * @param drawing - The drawing that contains the cells
         * @returns {HTMLDivElement} - The initialized cell
         */

        this.initCell = (drawing, color) => {
            const cell = document.createElement("div");
            cell.classList.add("cell");
            cell.style.backgroundColor = color;

            cell.onmouseenter = () => {
                if (drawing.ismousedown) {
                    doDraw(drawing, cell);
                }
            };
            cell.onmousedown = () => {
                doDraw(drawing, cell);
            };
            return cell;
        };
    }
}

/**
 * Drawing class creates a canvas with a specific background color and changes the color of the pen.
 */
class Drawing {
    constructor() {

        let currentBackgroundColor;

        const
            rows = 100;
        const
            columns = 100;
        /**
         * Checks if a color is a valid css color.
         * @param strColor - The input color
         * @returns {boolean} - Returns true if the color is valid, false otherwise.
         */
        let isColor = function(strColor){
            let s = new Option().style;
            s.color = strColor;
            return s.color === strColor;
        };


        /**
         * Initializes the control panel and adds it to the drawing
         * @param drawing - The drawing element
         */
        const
            initControlPanel = (drawing) => {

                //got the list of colors from https://stackoverflow.com/questions/1573053/javascript-function-to-convert-color-names-to-hex-codes
                let colors = {"aliceblue":"#f0f8ff","antiquewhite":"#faebd7","aqua":"#00ffff","aquamarine":"#7fffd4","azure":"#f0ffff",
                    "beige":"#f5f5dc","bisque":"#ffe4c4","black":"#000000","blanchedalmond":"#ffebcd","blue":"#0000ff","blueviolet":"#8a2be2","brown":"#a52a2a","burlywood":"#deb887",
                    "cadetblue":"#5f9ea0","chartreuse":"#7fff00","chocolate":"#d2691e","coral":"#ff7f50","cornflowerblue":"#6495ed","cornsilk":"#fff8dc","crimson":"#dc143c","cyan":"#00ffff",
                    "darkblue":"#00008b","darkcyan":"#008b8b","darkgoldenrod":"#b8860b","darkgray":"#a9a9a9","darkgreen":"#006400","darkkhaki":"#bdb76b","darkmagenta":"#8b008b","darkolivegreen":"#556b2f",
                    "darkorange":"#ff8c00","darkorchid":"#9932cc","darkred":"#8b0000","darksalmon":"#e9967a","darkseagreen":"#8fbc8f","darkslateblue":"#483d8b","darkslategray":"#2f4f4f","darkturquoise":"#00ced1",
                    "darkviolet":"#9400d3","deeppink":"#ff1493","deepskyblue":"#00bfff","dimgray":"#696969","dodgerblue":"#1e90ff",
                    "firebrick":"#b22222","floralwhite":"#fffaf0","forestgreen":"#228b22","fuchsia":"#ff00ff",
                    "gainsboro":"#dcdcdc","ghostwhite":"#f8f8ff","gold":"#ffd700","goldenrod":"#daa520","gray":"#808080","green":"#008000","greenyellow":"#adff2f",
                    "honeydew":"#f0fff0","hotpink":"#ff69b4",
                    "indianred ":"#cd5c5c","indigo":"#4b0082","ivory":"#fffff0","khaki":"#f0e68c",
                    "lavender":"#e6e6fa","lavenderblush":"#fff0f5","lawngreen":"#7cfc00","lemonchiffon":"#fffacd","lightblue":"#add8e6","lightcoral":"#f08080","lightcyan":"#e0ffff","lightgoldenrodyellow":"#fafad2",
                    "lightgrey":"#d3d3d3","lightgreen":"#90ee90","lightpink":"#ffb6c1","lightsalmon":"#ffa07a","lightseagreen":"#20b2aa","lightskyblue":"#87cefa","lightslategray":"#778899","lightsteelblue":"#b0c4de",
                    "lightyellow":"#ffffe0","lime":"#00ff00","limegreen":"#32cd32","linen":"#faf0e6",
                    "magenta":"#ff00ff","maroon":"#800000","mediumaquamarine":"#66cdaa","mediumblue":"#0000cd","mediumorchid":"#ba55d3","mediumpurple":"#9370d8","mediumseagreen":"#3cb371","mediumslateblue":"#7b68ee",
                    "mediumspringgreen":"#00fa9a","mediumturquoise":"#48d1cc","mediumvioletred":"#c71585","midnightblue":"#191970","mintcream":"#f5fffa","mistyrose":"#ffe4e1","moccasin":"#ffe4b5",
                    "navajowhite":"#ffdead","navy":"#000080",
                    "oldlace":"#fdf5e6","olive":"#808000","olivedrab":"#6b8e23","orange":"#ffa500","orangered":"#ff4500","orchid":"#da70d6",
                    "palegoldenrod":"#eee8aa","palegreen":"#98fb98","paleturquoise":"#afeeee","palevioletred":"#d87093","papayawhip":"#ffefd5","peachpuff":"#ffdab9","peru":"#cd853f","pink":"#ffc0cb","plum":"#dda0dd","powderblue":"#b0e0e6","purple":"#800080",
                    "rebeccapurple":"#663399","red":"#ff0000","rosybrown":"#bc8f8f","royalblue":"#4169e1",
                    "saddlebrown":"#8b4513","salmon":"#fa8072","sandybrown":"#f4a460","seagreen":"#2e8b57","seashell":"#fff5ee","sienna":"#a0522d","silver":"#c0c0c0","skyblue":"#87ceeb","slateblue":"#6a5acd","slategray":"#708090","snow":"#fffafa","springgreen":"#00ff7f","steelblue":"#4682b4",
                    "tan":"#d2b48c","teal":"#008080","thistle":"#d8bfd8","tomato":"#ff6347","turquoise":"#40e0d0",
                    "violet":"#ee82ee",
                    "wheat":"#f5deb3","white":"#ffffff","whitesmoke":"#f5f5f5",
                    "yellow":"#ffff00","yellowgreen":"#9acd32"};

                /**
                 * Checks if a color is in the list of colors
                 * @param color - The input color
                 * @param colors - list of colors
                 * @returns {boolean} - true if the color exists in the list, false otherwise.
                 */
                let isColorName = function(color, colors) {
                    return typeof colors[color.toLowerCase()] != 'undefined';

                };

                /**
                 * Gets the from the list of valid colors the rgb value of the keyword
                 */
                let getHexFromColor = function(color, colors){
                    if (typeof colors[color.toLowerCase()] != 'undefined')
                        return colors[color.toLowerCase()];
                    return false;
                };

                /**
                 * Delete error if it exists
                 */
                let deleteError = function () {
                    let errorElement = document.getElementById("paletteError");
                    if (errorElement !== null){
                        errorElement.parentNode.removeChild(errorElement);
                    }
                };

                const row = initRow();

                //creates a label that indicates user to choose a color for the pen
                const paletteExplain = document.createElement("label");
                paletteExplain.innerHTML = "Select Color for Pen:";
                row.appendChild(paletteExplain);

                //creates a color palette so that the use can select a color
                const colorPalette = document.createElement("input");
                colorPalette.type = "color";
                drawing.selectedColor = colorPalette.value;
                colorPalette.onchange = () => {
                    deleteError();
                    document.getElementById("manualColorInput").value = "";
                    drawing.selectedColor = colorPalette.value;

                };
                row.appendChild(colorPalette);

                //creates a label that explains instructions of manual input
                const paletteExplain2 = document.createElement("label");
                paletteExplain2.innerHTML = "Input a valid Hex Color or a valid Key Color. Ex: '#ffffff' or 'pink'";
                row.appendChild(paletteExplain2);

                //creates an eraser
                const eraser = document.createElement("button");
                eraser.innerHTML = "Eraser";
                eraser.onclick = () => {
                  drawing.selectedColor = currentBackgroundColor;
                };

                //creates the manual input
                const manualColorInput = document.createElement("input");
                manualColorInput.type = "text";
                manualColorInput.id = "manualColorInput";
                //creates event when user presses enter to get and select the color or show error code
                manualColorInput.addEventListener("keyup", function(event){
                    if (event.keyCode === 13){
                        let colorStr = manualColorInput.value;
                        //check if str is in rgb
                        if (/^#[0-9A-F]{6}$/i.test(colorStr)) {
                            deleteError();
                            drawing.selectedColor = colorStr;
                            colorPalette.value = colorStr;

                        //check if str is a color key
                        } else if (isColorName(colorStr, colors)){
                            deleteError();
                            drawing.selectedColor = colorStr;
                            colorPalette.value = getHexFromColor(colorStr, colors);

                        //create color error
                        } else {
                            if (document.getElementById("paletteError") === null){
                                const paletteError = document.createElement("label");
                                paletteError.innerHTML = colorStr + " is an invalid input";
                                paletteError.style.color = "red";
                                paletteError.id = "paletteError";
                                row.appendChild(paletteError);
                            } else {
                                document.getElementById("paletteError").innerHTML = colorStr + " is an invalid input";
                            }

                        }
                    }
                });

                row.appendChild(manualColorInput);


                drawing.appendChild(row);
                drawing.appendChild(eraser);

            };


        /**
         * Initializes the drawing and sets mouse events
         * @param drawing - The drawing element to init
         */
        const
            initDrawing = (drawing) => {
                drawing.ismousedown = false;
                drawing.onmousedown = () => {
                    drawing.ismousedown = true;
                };
                drawing.onmouseup = () => {
                    drawing.ismousedown = false;
                };
                initControlPanel(drawing);
            };

        /**
         * Initializes creates and initializes a new row
         * @returns {HTMLDivElement} - The initialized row
         */
        const
            initRow = () => {
                const row = document.createElement("div");
                row.classList.add("row");
                return row;
            };

        /**
         * Takes user input from a text box
         */
        this.input = function () {
            window.onload = function() {
                let colorInput = document.getElementById("colorInput");
            }
        };

        /**
         * Adds rows, creates cells, and adds cells to rows.
         */
        this.init = function() {
            let color = document.getElementById("colorInput");
            const drawings = document.getElementsByClassName("drawing");
            let errorElement = document.getElementById("colorError");
            //check if cells where created
            if (drawings.item(0).childNodes.item(0) === null){

                if( !isColor(color.value) || color.value === ''){
                    errorElement.innerText = color.value + " is not a valid color.";
                    errorElement.style.color = "red";
                    errorElement.focus();
                } else {
                    errorElement.innerText = " ";
                    // Loop through all drawings on the page
                    Array.from(drawings).forEach((drawing) => {
                        initDrawing(drawing);

                        let cell = new Cell();
                        // Add rows
                        for (let i = 0; i < rows; i++) {
                            const row = initRow();
                            drawing.appendChild(row);

                            // Create the cells and add them to the row
                            for (let j = 0; j < columns; j++) {
                                row.appendChild(cell.initCell(drawing, color.value));
                            }
                        }
                    });
                    currentBackgroundColor = color.value;
                }

            } else {
                if( !isColor(color.value) || color.value === ''){
                    errorElement.innerText = color.value + " is not a valid color.";
                    errorElement.style.color = "red";
                    errorElement.focus();
                } else {
                    errorElement.innerText = " ";
                    let cells = document.getElementsByClassName("cell");
                    for (let i = 0; i < cells.length; i++) {
                        cells.item(i).style.backgroundColor = color.value;
                    }

                    currentBackgroundColor = color.value;
                }
            }


        }

    }
}